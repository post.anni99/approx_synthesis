// Copyright (C) AGRA - University of Bremen
//
// LICENSE : GNU GPLv3
// 
// @author : Arun <arun@uni-bremen.de>
// @file   : common_utils.hpp
//------------------------------------------------------------------------------
#pragma once

#ifndef COMMON_UTILS_HPP
#define COMMON_UTILS_HPP

#include <fstream>
#include <iostream>
#include <string>
#include <ctime>
#include <boost/filesystem.hpp>
#include <boost/dynamic_bitset.hpp>
#include <iterator>

namespace minikit {
  
void create_work_dir (const std::string &dir_name);
void delete_work_dir (const std::string &dir_name);
char* curr_time ();
float get_elapsed_time ( const clock_t &begin_time );
void print_dbg (const std::string &str);
void cat_two_files ( const std::string &ofile, const std::string ifile1, 
		     const std::string ifile2 );
void cat_three_files ( const std::string &ofile, const std::string &ifile1, 
		       const std::string &ifile2, const std::string &ifile3 );

std::string get_extension_of_file (const std::string &file_name);
std::string get_name_of_file (const std::string &file_name);

}
#endif
//------------------------------------------------------------------------------

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
