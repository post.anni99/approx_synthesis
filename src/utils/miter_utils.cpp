// Copyright (C) AGRA - University of Bremen
//
// LICENSE : GNU GPLv3
// 
// @author : Arun <arun@uni-bremen.de>
// @file   : miter_utils.cpp
// @brief  : Utilities for writing verilog miter circuit. 
//------------------------------------------------------------------------------

#include "miter_utils.hpp"

namespace minikit
{
//------------------------------------------------------------------------------
// Support functions for MITER
//------------------------------------------------------------------------------
inline void write_packing__miter ( std::ofstream &of, const Design &orig,
				   const Port &port_to_check ) {

  of << "\nwire " << get_port_width_str (port_to_check)
     << "packed_orig, packed_appx, diff, diff_wc, diff_bf ;" << std::endl;
  if ( 1 == get_port_width (port_to_check) ) {
    of << "assign packed_orig  = " << get_port_name (port_to_check) << "_orig ;\n";

    of << "assign packed_appx  = " << get_port_name (port_to_check) << "_appx ;\n";
  } else {
    std::string pack_str_orig = " ", pack_str_appx = " ";
    for (int i = get_port_width (port_to_check) - 1 ; i > 0; i-- ) {
      pack_str_orig = pack_str_orig + "\\" + get_port_name (port_to_check) + "["
	+ std::to_string (i) + "]_orig , "; 
      pack_str_appx = pack_str_appx + "\\" + get_port_name (port_to_check) + "["
	+ std::to_string (i) + "]_appx , "; 
    }
    pack_str_orig = pack_str_orig + "\\" + get_port_name (port_to_check) + "[0]_orig "; 
    pack_str_appx = pack_str_appx + "\\" + get_port_name (port_to_check) + "[0]_appx "; 
      
    of << "assign packed_orig  = { " << pack_str_orig << " } ;\n";
    of << "assign packed_appx  = { " << pack_str_appx << " } ;\n";

  }
}

inline void write_header__miter ( std::ofstream &of, const Design &orig,
				  const Port &port_to_check ) {
  std::string valid = "valid";
  for ( auto &pi_port : get_design_pi_list(orig) ) {
    of << get_port_name (pi_port) << " , ";
  }
  of << valid << " );" << std::endl;

  for ( auto &pi_port : get_design_pi_list(orig) ) {
    of << "input " << get_port_width_str (pi_port) << get_port_name (pi_port)
       << " ;" << std::endl;
  }
  of << "output " << valid << " ; " << std::endl << std::endl;

  for ( auto &po_port : get_design_po_list (orig) ) {
    of << "wire " << get_port_width_str (po_port)
       << get_port_name (po_port) + "_orig" << " ;" << std::endl;
    of << "wire " << get_port_width_str (po_port)
       << get_port_name (po_port) + "_appx" << " ;" << std::endl;
  }

}

inline void write_instance__miter ( std::ofstream &of, const Design &orig,
				    const Port &port_to_check ) {
  of << std::endl << "\n// Instantiation " << std::endl;
  
  of << get_design_name (orig) << "  inst_" << get_design_name (orig) << " (\n";
  auto pi_list = get_design_pi_list(orig);
  for ( auto &pi : pi_list ) {
    of << "." << get_port_name (pi) << " ( " << get_port_name (pi) << " ),\n";
  }
  auto po_list = get_design_po_list(orig);
  for (auto i = 0u;  i < po_list.size() - 1; i++) {
    of << "." << get_port_name ( po_list[i] ) << " ( "
       << get_port_name ( po_list[i] ) << "_orig ),\n";
  }
  of << "." << get_port_name ( po_list [po_list.size() - 1] ) << " ( " 
     << get_port_name ( po_list [po_list.size() - 1] ) << "_orig )\n";
  of << ");\n" << std::endl;
  

  of << get_design_name (orig) << "_appx  inst_"
     << get_design_name (orig) << "_appx (\n";
  pi_list = get_design_pi_list(orig);
  for ( auto &pi : pi_list ) {
    of << "." << get_port_name (pi) << " ( " << get_port_name (pi) << " ),\n";
  }
  po_list = get_design_po_list(orig);
  for (auto i = 0u;  i < po_list.size() - 1; i++) {
    of << "." << get_port_name ( po_list[i] ) << " ( "
       << get_port_name ( po_list[i] ) << "_appx ),\n";
  }
  of << "." << get_port_name ( po_list [po_list.size() - 1] ) << " ( " 
     << get_port_name ( po_list [po_list.size() - 1] ) << "_appx )\n";
  of << ");\n" << std::endl << std::endl << std::endl;

}

inline void write_wc_logic__miter ( std::ofstream &of, const unsigned &limit ) {
  of << "assign diff = (packed_orig > packed_appx) ? \n"
     <<	"       (packed_orig - packed_appx) : (packed_appx - packed_orig);\n";
  of << "assign valid = (diff > " << std::to_string (limit)
     << ") ? 1 : 0;\n" << std::endl;
}


inline void write_bf_logic__miter ( std::ofstream &of, const unsigned &limit, 
				    const Port &port_to_check ) {
  //assert (false); 
  of << "assign diff = (packed_orig ^ packed_appx); \n";

  of << "assign valid = ( (";

  // problem is a huge xor-add tree. Somehow tool is taking long time for this. 
  // may be DAC work has other bugs, for about 100 output bits benchmark.
  for (int i = get_port_width(port_to_check) - 1; i > 0;  i-- ) {
    of << "diff[" << i << "] + ";
  }
  of << "diff[0] )  > " << std::to_string (limit)
     << ") ? 1 : 0;\n" << std::endl;

}

inline void write_ge_bf_logic__miter ( std::ofstream &of, const unsigned &limit, 
				       const Port &port_to_check ) {
  //assert (false); 
  of << "assign diff = (packed_orig ^ packed_appx); \n";

  of << "assign valid = ( (";

  // problem is a huge xor-add tree. Somehow tool is taking long time for this. 
  // may be DAC work has other bugs, for about 100 output bits benchmark.
  for (int i = get_port_width(port_to_check) - 1; i > 0;  i-- ) {
    of << "diff[" << i << "] + ";
  }
  of << "diff[0] )  >= " << std::to_string (limit)
     << ") ? 1 : 0;\n" << std::endl;

}


inline void write_wc_bf_logic__miter ( std::ofstream &of, const unsigned &wc_limit, 
				       const unsigned &bf_limit, const Port &port_to_check ) {
  // WORST CASE part.
  of << "assign diff_wc = (packed_orig > packed_appx) ? \n"
     <<	"       (packed_orig - packed_appx) : (packed_appx - packed_orig);\n";

  // BIT FLIP part.
  of << "assign diff_bf = (packed_orig ^ packed_appx); \n";

  // Combined Valid part.
  of << "assign valid = ( (diff_wc > " << std::to_string (wc_limit)
     << ") ? 1 : 0  )  | ( " ;

  of << " ( (";
  for (int i = get_port_width(port_to_check) - 1; i > 0;  i-- ) {
    of << "diff_bf[" << i << "] + ";
  }
  of << "diff_bf[0] )  > " << std::to_string (bf_limit)
     << ") ? 1 : 0 );\n" << std::endl;

}


//---inline std::string wc__predicate__miter ( const Port &port ) {
//---  return ( get_port_name(port) );
//---}


//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
// MITERS.

void write_worst_case_miter (
  const Design &orig, const Port &port_to_check,
  const std::string &miter_module, const std::string &ofile, const unsigned &limit)
{

//  assert (false);
//--- do properly with lambda for this
//---  auto result = std::find_if (
//---    (get_design_po_list(orig)).begin(), (get_design_po_list(orig)).end(),
//---    wc__predicate__miter );
//---  assert ( result == (get_design_po_list(orig)).end()
//---	   && "[e] Error: port_to_check not found in output-port list!\n"  );


  assert ( (get_port_width (port_to_check) <= 128) 
	   && "[e] Port of width more than 128 NOT supported!" );

  
  std::string valid = "valid";
  std::ofstream of;
  of.open (ofile);
  of << "// Worst Case Error Miter." << std::endl;
  of << "module " + miter_module + " ( ";            // 0. Start module ...
  write_header__miter   ( of, orig, port_to_check ); // 1. Header and declaration.
  write_packing__miter  ( of, orig, port_to_check ); // 2. Write packing logic
  write_instance__miter ( of, orig, port_to_check ); // 3. Instantiate appx and orig
  write_wc_logic__miter ( of, limit );               // 4. Write the miter logic
  of << "endmodule // " + miter_module << std::endl; // 5. Concluding endmodule
  of.close();
  
}


void write_bit_flip_miter (
  const Design &orig, const Port &port_to_check,
  const std::string &miter_module, const std::string &ofile, const unsigned &limit)
{

  assert ( (get_port_width (port_to_check) <= 128) 
	   && "[e] Port of width more than 128 NOT supported!" );

  
  std::string valid = "valid";
  std::ofstream of;
  of.open (ofile);
  of << "// Bit Flip Error Miter." << std::endl;
  of << "module " + miter_module + " ( ";            // 0. Start module ...
  write_header__miter   ( of, orig, port_to_check ); // 1. Header and declaration.
  write_packing__miter  ( of, orig, port_to_check ); // 2. Write packing logic
  write_instance__miter ( of, orig, port_to_check ); // 3. Instantiate appx and orig
  write_bf_logic__miter ( of, limit, port_to_check );// 4. Write the miter logic
  of << "endmodule // " + miter_module << std::endl; // 5. Concluding endmodule
  of.close();
  
}

// Error is checked for >= ( all other cases its > )
void write_ge_bit_flip_miter (
  const Design &orig, const Port &port_to_check,
  const std::string &miter_module, const std::string &ofile, const unsigned &limit)
{

  assert ( (get_port_width (port_to_check) <= 128) 
	   && "[e] Port of width more than 128 NOT supported!" );

  
  std::string valid = "valid";
  std::ofstream of;
  of.open (ofile);
  of << "// Bit Flip Error Miter." << std::endl;
  of << "module " + miter_module + " ( ";            // 0. Start module ...
  write_header__miter   ( of, orig, port_to_check ); // 1. Header and declaration.
  write_packing__miter  ( of, orig, port_to_check ); // 2. Write packing logic
  write_instance__miter ( of, orig, port_to_check ); // 3. Instantiate appx and orig
  write_ge_bf_logic__miter ( of, limit, port_to_check );// 4. Write the miter logic
  of << "endmodule // " + miter_module << std::endl; // 5. Concluding endmodule
  of.close();
  
}



void write_wc_bf_miter ( const Design &orig, const Port &port_to_check,
			 const std::string &miter_module, const std::string &ofile, 
			 const unsigned &wc_limit, const unsigned &bf_limit)
{

  assert ( (get_port_width (port_to_check) <= 128) 
	   && "[e] Port of width more than 128 NOT supported!" );

  
  std::string valid = "valid";
  std::ofstream of;
  of.open (ofile);
  of << "// Worst Case + Bit Flip Error Miter." << std::endl;
  of << "module " + miter_module + " ( ";            // 0. Start module ...
  write_header__miter   ( of, orig, port_to_check ); // 1. Header and declaration.
  write_packing__miter  ( of, orig, port_to_check ); // 2. Write packing logic
  write_instance__miter ( of, orig, port_to_check ); // 3. Instantiate appx and orig
  write_wc_bf_logic__miter ( of, wc_limit, bf_limit, port_to_check );// 4. Write the miter logic
  of << "endmodule // " + miter_module << std::endl; // 5. Concluding endmodule
  of.close();
  
}




//------------------------------------------------------------------------------
}

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
