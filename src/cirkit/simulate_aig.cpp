/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 * Copyright (C) 2015-2016  EPFL
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Arun : Highly trimmed down version of original cirkit functions.

#include "simulate_aig.hpp"

namespace cirkit
{

/******************************************************************************
 * BDD simulation                                                             *
 ******************************************************************************/

BDD bdd_simulator::get_input( const aig_node& node, const std::string& name, unsigned pos, const aig_graph& aig ) const
{
  return mgr.bddVar( pos );
}

BDD bdd_simulator::get_constant() const
{
  return mgr.bddZero();
}

BDD bdd_simulator::invert( const BDD& v ) const
{
  return !v;
}

BDD bdd_simulator::and_op( const aig_node& node, const BDD& v1, const BDD& v2 ) const
{
  return v1 & v2;
}

}

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
