/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 * Copyright (C) 2015-2016  EPFL
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file simulate_aig.hpp
 *
 * @brief AIG simuation
 *
 * @author Mathias Soeken
 * @since  2.0
 */

// Arun : Highly trimmed down version of original cirkit functions.

#ifndef SIMULATE_AIG_HPP
#define SIMULATE_AIG_HPP

#include <functional>
#include <map>
#include <unordered_map>

#include <boost/graph/depth_first_search.hpp>
#include <boost/property_map/property_map.hpp>
#include <boost/range/algorithm.hpp>
#include <boost/range/algorithm_ext/push_back.hpp>

#include <cirkit/properties.hpp>
#include <cirkit/aig.hpp>
#include <cirkit/aig_dfs.hpp>
#include <cirkit/aig_utils.hpp>

#include <cuddObj.hh>

namespace cirkit
{

/******************************************************************************
 * Abstract class for simulators                                              *
 ******************************************************************************/

template<typename T>
class aig_simulator
{
public:
  /**
   * @brief Simulator routine when input is encountered
   *
   * @param node AIG node reference in the `aig' graph
   * @param name Name of that node
   * @param pos  Position of that node (usually wrt. to `aig' inputs vector)
   * @param aig  AIG graph
   */
  virtual T get_input( const aig_node& node, const std::string& name, unsigned pos, const aig_graph& aig ) const = 0;
  virtual T get_constant() const = 0;
  virtual T invert( const T& v ) const = 0;
  virtual T and_op( const aig_node& node, const T& v1, const T& v2 ) const = 0;

  virtual bool terminate( const aig_node& node, const aig_graph& aig ) const
  {
    return false;
  }
};


class bdd_simulator : public aig_simulator<BDD>
{
public:
  bdd_simulator() : mgr( Cudd() ) {}
  bdd_simulator( const Cudd& mgr ) : mgr( mgr ) {}

  BDD get_input( const aig_node& node, const std::string& name, unsigned pos, const aig_graph& aig ) const;
  BDD get_constant() const;
  BDD invert( const BDD& v ) const;
  BDD and_op( const aig_node& node, const BDD& v1, const BDD& v2 ) const;

protected:
  Cudd mgr;
};


/******************************************************************************
 * DFS visitor for actual simulation                                          *
 ******************************************************************************/

using aig_node_color_map = circuit_traits<aig_graph>::node_color_map;

template<typename T>
struct simulate_aig_node_visitor : public aig_dfs_visitor
{
public:
  simulate_aig_node_visitor( const aig_graph& aig, const aig_simulator<T>& simulator, std::map<aig_node, T>& node_values )
    : aig_dfs_visitor( aig ),
      simulator( simulator ),
      node_values( node_values ) {}

  void finish_input( const aig_node& node, const std::string& name, const aig_graph& aig )
  {
    unsigned pos = std::distance( graph_info.inputs.begin(), boost::find( graph_info.inputs, node ) );
    node_values[node] = simulator.get_input( node, name, pos, aig );
  }

  void finish_constant( const aig_node& node, const aig_graph& aig )
  {
    node_values[node] = simulator.get_constant();
  }

  void finish_aig_node( const aig_node& node, const aig_function& left, const aig_function& right, const aig_graph& aig )
  {
    T tleft, tright;

    const auto itleft  = node_values.find( left.node );
    const auto itright = node_values.find( right.node );

    if ( itleft != node_values.end() )
    {
      tleft = left.complemented ? simulator.invert( itleft->second ) : itleft->second;
    }
    if ( itright != node_values.end() )
    {
      tright = right.complemented ? simulator.invert( itright->second ) : itright->second;
    }

    node_values[node] = simulator.and_op( node, tleft, tright );
  }

private:
  const aig_simulator<T>& simulator;
  std::map<aig_node, T>& node_values;
};

/******************************************************************************
 * Methods to trigger simulation                                              *
 ******************************************************************************/

template<typename T>
T simulate_aig_node( const aig_graph& aig, const aig_node& node,
                     const aig_simulator<T>& simulator,
                     aig_node_color_map& colors,
                     std::map<aig_node, T>& node_values )
{
  boost::depth_first_visit( aig, node,
                            simulate_aig_node_visitor<T>( aig, simulator, node_values ),
                            boost::make_assoc_property_map( colors ),
                            [&simulator]( const aig_node& node, const aig_graph& aig ) { return simulator.terminate( node, aig ); } );

  return node_values[node];
}

template<typename T>
T simulate_aig_node( const aig_graph& aig, const aig_node& node,
                     const aig_simulator<T>& simulator )
{
  aig_node_color_map colors;
  std::map<aig_node, T> node_values;

  return simulate_aig_node<T>( aig, node, simulator, colors, node_values );
}

template<typename T>
T simulate_aig_function( const aig_graph& aig, const aig_function& f,
                         const aig_simulator<T>& simulator,
                         aig_node_color_map& colors,
                         std::map<aig_node, T>& node_values )
{
  T value = simulate_aig_node<T>( aig, f.node, simulator, colors, node_values );
  return f.complemented ? simulator.invert( value ) : value;
}

template<typename T>
T simulate_aig_function( const aig_graph& aig, const aig_function& f,
                         const aig_simulator<T>& simulator )
{
  T value = simulate_aig_node<T>( aig, f.node, simulator );
  return f.complemented ? simulator.invert( value ) : value;
}

template<typename T>
std::map<aig_function, T> simulate_aig( const aig_graph& aig, const aig_simulator<T>& simulator,
                                        const properties::ptr& settings = properties::ptr(),
                                        const properties::ptr& statistics = properties::ptr() )
{
  /* settings */
  auto verbose = get( settings, "verbose", false );

  /* timer */
  //XXX properties_timer t( statistics );

  aig_node_color_map colors;
  std::map<aig_node, T> node_values;

  std::map<aig_function, T> results;

  for ( const auto& o : aig_info( aig ).outputs )
  {
    if ( verbose )
    {
      std::cout << "[i] simulate '" << o.second << "'" << std::endl;
    }
    T value = simulate_aig_node<T>( aig, o.first.node, simulator, colors, node_values );

    /* value may need to be inverted */
    results[o.first] = o.first.complemented ? simulator.invert( value ) : value;
  }

  set( statistics, "node_values", node_values );

  return results;
}

}

#endif

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
