// Copyright (C) AGRA - University of Bremen
//
// LICENSE : GNU GPLv3
// 
// @author : Arun <arun@uni-bremen.de>
// @file   : appx_miter.hpp
// @brief  : functions to create, solve appx miters.
//
// 
//------------------------------------------------------------------------------
#pragma once
#ifndef APPX_MITER_HPP
#define APPX_MITER_HPP

#include <cassert>
#include <iostream>
#include <algorithm>
#include <boost/filesystem.hpp>

#include <utils/miter_utils.hpp>
#include <utils/common_utils.hpp>
#include <utils/abc_utils.hpp>
#include <utils/yosys_utils.hpp>

#include <types/Path.hpp>
#include <types/AbcTypes.hpp>
#include <types/AppxSynthesisTypes.hpp>

#include <functions/has_limit_crossed.hpp>
#include <functions/path_routines.hpp>
#include <functions/cut_routines.hpp>

namespace minikit {

void init_appx_miter (const std::string &dir_work_path, const unsigned &debug_val,
		      const std::string &orig_verilog);
SolverResult solve_wc_appx_miter (const Design &design, const Port &port_to_check, 
				  const unsigned &limit, Network work_ntk);
SolverResult solve_bf_appx_miter (const Design &design, const Port &port_to_check, 
				  const unsigned &limit, Network work_ntk);
SolverResult solve_wc_bf_appx_miter (const Design &design, const Port &port_to_check, 
				     const unsigned &wc_limit, const unsigned &bf_limit, 
				     Network work_ntk);
SolverResult solve_ge_bf_appx_miter (const Design &design, const Port &port_to_check, 
				     const unsigned &limit, Network work_ntk);

}

#endif

//------------------------------------------------------------------------------
// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
