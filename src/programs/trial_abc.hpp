// Copyright (C) AGRA - University of Bremen
//
// LICENSE : GNU GPLv3
// 
// @author : Arun <arun@uni-bremen.de>
// @file   : trial_abc.hpp
//------------------------------------------------------------------------------
#pragma once
#ifndef TRIAL_ABC_HPP
#define TRIAL_ABC_HPP

#include <iostream>
#include <utility>
#include <cstdlib>
#include <cstdio>

#include <utils/program_options.hpp>
#include <utils/common_utils.hpp>
#include <utils/yosys_utils.hpp>
#include <utils/miter_utils.hpp>
#include <utils/abc_utils.hpp>

#include <ext-libs/abc/abc_api.hpp>
#include <ext-libs/yosys/yosys_api.hpp>

#include <functions/has_limit_crossed.hpp>

namespace minikit {

}


#endif

//------------------------------------------------------------------------------
// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
