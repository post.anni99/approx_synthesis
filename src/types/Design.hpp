// Copyright (C) AGRA - University of Bremen
//
// LICENSE : GNU GPLv3
// 
// @author : Arun <arun@uni-bremen.de>
// @file   : Design.hpp
// @brief  : Definition and inline functions for Design 
//
// Design is a tuple <design-name, collection-of-pi, collection-of-po>
// It simply holds the relevant design info.
//------------------------------------------------------------------------------
#pragma once
#ifndef DESIGN_HPP
#define DESIGN_HPP

#include <types/Port.hpp>
#include <tuple>
#include <vector>

namespace minikit {
using Design = std::tuple < std::string,       // Primary-Input
			    std::vector<Port>, // Collection of PI
			    std::vector<Port>  // Collection of PO
			    >;

inline Design  make_design ( const std::string &name,
			     const std::vector<Port> &pi_list,
			     const std::vector<Port> &po_list ) {
  return std::make_tuple (name, pi_list, po_list);
}

// These are simple slicing operations on tuple.
inline std::vector<Port> get_design_pi_list (const Design &design) {
  return std::get<1> (design);
}
inline std::vector<Port> get_design_po_list (const Design &design) {
  return std::get<2> (design);
}
inline std::string get_design_name (const Design &design) {
  return std::get<0> (design);
}

}


#endif

//------------------------------------------------------------------------------
// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
