// Copyright (C) AGRA - University of Bremen
//
// LICENSE : GNU GPLv3
// 
// @author : Arun <arun@uni-bremen.de>
// @file   : Port.hpp
// @brief  : Definition and inline functions for Port 
//
// Port is a pair <name, width>
//------------------------------------------------------------------------------
#pragma once
#ifndef PORT_HPP
#define PORT_HPP

#include <utility>
#include <vector>
#include <cassert>

namespace minikit {
using Port = std::pair < std::string,  // Port name
			 unsigned >;   // Port width

inline Port make_port (const std::string &name, const unsigned &width) {
  // Port width more than 128 not supported. To catch -ve number and improper widths.
  assert ( width > 0 && width < 129 ); 
  return std::make_pair (name, width);
}

inline unsigned get_port_width (const Port &port) {
  return port.second;
}

inline std::string get_port_width_str (const Port &port) {
  auto width = port.second;
  return (1 == width) ? " " : ("  [" + std::to_string(width - 1) + ":0]  ") ;
}

inline std::string get_port_name (const Port &port) {
  return port.first;
}

}


#endif

//------------------------------------------------------------------------------
// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
