// Copyright (C) AGRA - University of Bremen
//
// LICENSE : GNU GPLv3
// 
// @author : Arun <arun@uni-bremen.de>
// @file   : Path.hpp
// @brief  : Definition and inline functions for Path 
//
// Path is a tuple <PI, collection-of-nodes, PO>
// Ended up in giving meaningful names for tuple operations.
// For performance dont want to put another class wrapper and heavily inlined.
// Disavantage is not checking if its really PI/PO etc. But extensible for CI/CO
//------------------------------------------------------------------------------
#pragma once
#ifndef PATH_HPP
#define PATH_HPP

#include <tuple>
#include <vector>

#include <ext-libs/abc/abc_api.hpp>
#include <types/AbcTypes.hpp>

namespace minikit {
using Path = std::tuple < abc::Abc_Obj_t *,  // Primary-Input
			  std::vector<abc::Abc_Obj_t *>, // Collection of Nodes
			  abc::Abc_Obj_t* >; // Primary-Output

// These are simple slicing operations on tuple.
inline abc::Abc_Obj_t * get_pi_of_path (const Path &path) {
  return std::get<0> (path);
}
inline abc::Abc_Obj_t * get_po_of_path (const Path &path) {
  return std::get<2> (path);
}
inline std::vector <abc::Abc_Obj_t *> get_nodes_of_path (const Path &path)
{
  return std::get<1> (path);
}

// Add an extra node to the given path. 
// Does NOT unique. Its the callers responsibility to insert a unique node if needed.
inline void add_node_to_path (Path &path, Object &node) {
  assert (is_object_good (node));
  // make sure its not PI,PO, const etc.
  if ( (node == nullptr) || !abc::Abc_ObjIsNode(node) ) return ;
  if ( abc::Abc_ObjIsPi(node) ) return ;
  if ( abc::Abc_ObjIsPo(node) ) return ;
  if ( abc::Abc_AigNodeIsConst(node) ) return ;

  //std::get<1> path = (std::get<1> path).emplace_back (node);
  //(std::get<1> path).emplace_back (node);
  //auto node_list = std::get<1> path;
  std::vector<abc::Abc_Obj_t *> node_list = get_nodes_of_path (path);
  node_list.push_back (node);
  std::get<1> (path) = node_list;
  // TODO OPTIMIZE THIS ONE.
}

}


#endif

//------------------------------------------------------------------------------
// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
