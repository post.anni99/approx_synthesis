#!/usr/bin/env bash
# @usage  : Cleanup the current build. Plain ninja clean is not enough.
#         : Note - deletes all the existing build so far. Use with caution!

if [ ! -e ./build ]; then 
    printf "[e] Invoke ONLY from PROJECT-ROOT directory\n"; 
    printf "[i] Usage: ./utils/clean_build.sh\n";
    exit; 
fi

if [ ! -z $1 ]; then 
    printf "[i] Usage: ./utils/clean_build.sh\n";
    printf "[w] Deletes everything built so far! \n";
    printf "[i] Startover with :: cd ./build; cmake .. -G Ninja; ninja external; ninja appx_synthesis\n";
    printf "[w] ninja external alone can take upto 1 hour! \n";
    exit
fi


printf "[w] Deletes everything built so far! \n";
printf "[i] Startover with :: cd ./build; cmake .. -G Ninja;  ninja external; ninja appx_synthesis\n";
printf "[w] ninja external alone can take upto 1 hour! \n";
read -p  "[i] Enter to continue. Ctrl-C to exit : "
#-------------------------------------------------------------------------------
cd ext
\rm -rf include lib boost/stage boost/boost
cd ../build
\rm -rf ext CMakeFiles *.ninja src programs
\rm -f CMakeCache.txt cmake_install.cmake
\rm -f .ninja_deps .ninja_log
cd ..
