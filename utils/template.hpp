// Copyright (C) AGRA - University of Bremen
//
// LICENSE : GNU GPLv3
// 
// @author : Arun <arun@uni-bremen.de>
// @file   : template.hpp
// @brief  : 
//
// 
//------------------------------------------------------------------------------
#pragma once
#ifndef TEMPLATE_HPP
#define TEMPLATE_HPP

#include <cassert>
#include <iostream>
#include <algorithm>

#include <ext-libs/abc/abc_api.hpp>
#include <types/Path.hpp>
#include <types/AbcTypes.hpp>
#include <types/AppxSynthesisTypes.hpp>
#include <utils/abc_utils.hpp>

namespace minikit {

}

#endif

//------------------------------------------------------------------------------
// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
